# DKX/Http/Middleware/Mount

Mount middleware for [@dkx/http-server](https://gitlab.com/dkx/http/server).

## Installation

```bash
$ npm install --save @dkx/http-middleware-mount
```

or with yarn

```bash
$ yarn add @dkx/http-middleware-mount
```

## Usage

```js
const {Server} = require('@dkx/http-server');
const {mountMiddleware} = require('@dkx/http-middleware-mount');

const app = new Server;
const appA = new Server;
const appB = new Server;

app.use(mountMiddleware('/a', appA));
app.use(mountMiddleware('/b', appB));
```
