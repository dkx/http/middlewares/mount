import {Server, Middleware, Request, RequestState, Response, NextMiddlewareFunction} from '@dkx/http-server';


export function mountMiddleware(path: string, app: Server): Middleware
{
	if (path[0] !== '/') {
		throw new Error('Mount path must begin with "/"');
	}

	return async function(req: Request, res: Response, next: NextMiddlewareFunction, state: RequestState): Promise<Response>
	{
		const newPath = matching(path, req.url);

		if (newPath === null) {
			return next(res);
		}

		const innerState = {...state};
		const newReq = forkRequest(req, newPath);
		const newRes = forkResponse(res);

		res = await app.middleware(newReq, newRes, innerState);

		state.mount = {
			path,
			innerState,
			url: newPath,
		};

		return next(res);
	};
}


function forkRequest(req: Request, url: string): Request
{
	return new Request(req.method, url, req.headers, req.body);
}


function forkResponse(res: Response): Response
{
	return res.clone();
}


function matching(path: string, match: string): string|null
{
	if (match.indexOf(path) !== 0) {
		return null;
	}

	const trailingSlash = path.slice(-1) === '/';
	const newPath = match.replace(path, '') || '/';

	if (trailingSlash) {
		return newPath;
	}

	if (newPath[0] !== '/') {
		return null;
	}

	return newPath;
}
