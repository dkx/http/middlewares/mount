import {RequestState, Server, testMiddleware} from '@dkx/http-server';
import {responseMiddleware} from '@dkx/http-middleware-response';
import {expect} from 'chai';
import {mountMiddleware} from '../../lib';


let app: Server;


describe('#mountMiddleware', () => {

	beforeEach(() => {
		app = new Server;
	});

	it('should throw an error if path does not start with "/"', () => {
		expect(() => {
			mountMiddleware('', app);
		}).to.throw(Error, 'Mount path must begin with "/"');
	});

	it('should not match path', async () => {
		app.use(responseMiddleware({
			statusMessage: 'mounted',
		}));

		const res = await testMiddleware(mountMiddleware('/a', app), {
			url: '/b',
		});

		expect(res.statusMessage).to.be.equal('');
	});

	it('should not match different path', async () => {
		app.use(responseMiddleware({
			statusMessage: 'mounted',
		}));

		const res = await testMiddleware(mountMiddleware('/a', app), {
			url: '/ab',
		});

		expect(res.statusMessage).to.be.equal('');
	});

	it('should match sub path', async () => {
		const state: RequestState = {
			message: 'hello world',
		};

		app.use(responseMiddleware({
			statusMessage: 'mounted',
			onRequest: (request, state) => {
				expect(request.url).to.be.equal('/sub');
				expect(state).to.be.eql({
					message: 'hello world',
				});
			},
		}));

		const res = await testMiddleware(mountMiddleware('/a', app), {
			state,
			url: '/a/sub',
		});

		expect(res.statusMessage).to.be.equal('mounted');
		expect(state).to.be.eql({
			message: 'hello world',
			mount: {
				path: '/a',
				url: '/sub',
				innerState: {
					message: 'hello world',
				},
			},
		});
	});

});
